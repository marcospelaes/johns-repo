- git init
	Creates a Git repository in the current folder/directory

- git status
	View the status of each file in a repository

- git add [file] [.]
	Stage a file to the next commit

- git commit
	Commit the staged files with a descriptive message

- git log
	View a repository's commit history

- git config --global user.name [name]
	Defines the author name to be used in all repositories

- git config --global user.email [email]
	Defines the author e-mail to be used in all repositories

- git checkout [commit-id]
	View a previous commit

- git tag -a [tag-name] -m "description"
	Create an annotated tag pointing to the most recent commit

- git revert [commit-id]
	Undo the specified commit by applying a new commit

-git reset --hard
	Reset tracked files to match the most recent commit

- git clean -f
	Remove untracked files

-git reset --hard/git clean -f
	Permanetly undo uncommitted changes.

- git branch
	Lists all branches

- git branch <branch-name>
	Creates a new branch using the current working directory as its base

- git checkout <branch-name>
	Makes the working directory and HEAD macth the specified branch

- git merge <branch-name>
	Merge a branch into the checked-out branch

- git branch -d <branch-name>
	Delete a branch

- git rm <file> 
	Removes a file from the working directory (if applicable) ad stop tracking the file

- git commit -a -m "message"
	Stages all tracked files and commit the snapshot using the specified message

- git branch -D <branch-name>
	Froce the removal of an unmerged branch (be careful: it will be lost forever).

- git rebase <new-base>
	Move the current branch's commits to the tip of <new-base>, which can be either a branch name or a commit ID.

- git reabse -i <new-base>
	Perform an interactive rebase and select actions for each commit.
- git commit --amend
	Add staged changes to the most recent commit intead of creating a new one.

- git rebase --continue
	Continue a reabse after amend a commit.

- git rebase --abort
	Abandon the current interactive rebase and return to the repository to its former state.

- git merge --no-ff <branch-name>
	Force a merge commit even if Git could do a fast-forward merge.

- git reflog
	Displaye the local, chronological history of a repository.

- git reset --mixed HEAD~<n>
	Moves the head backward <n> commits, but don't change the working directory

- git reset --hard HEAD~<n>
	Moves the HEAD backward <n> commits and change the working directory to match.

- git log <since>..<until>
	Display the commits reachable from <until> but not from <since>. These parameters can be either commit ID's or branch names.

- git log --stat
	Inclue extra information about altered files in the log output.

- git clone <remote-path>
	Creates a copy of a remote Git repository.

- git remote
	List remote repositories.

- git remote add <remote-name> <remote-path>
	Add a remote repository.

- git fetch <remote-name>
	Download remote branch information, but not merge anything.

- git merge <remote-name>/<branch-name>
	Merge remote branch into the checked-out branch.

- git branch -r
	List remote branches.

- git push <remote-name> <branch-name>
	Push a local branch to another repository.

- git push <remote-name> <tag-name>
	Puch a tag to another repository.

- git init --bare <repository-name>
	Creates a Git repository, but omit the working directory.

- git remote rm <remote-name>
	Removes the specified remote from your bookmarked connections.


